﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager GM;
    public GameObject panelLose;
    public TankStats tankStats;

    public Transform farmParent;
    public Farm_Net[] farmPrefab;

    public float[] expTarget = {
        10f, 20f, 30f, 40f, 55f, 60f, 75f, 90f, 105f,
        135f, 165f, 195f, 225f, 255f, 285f, 315f, 345f, 375f, 405f,
        455f, 505f, 555f, 605f, 655f, 705f, 755f, 805f, 855f, 905f
    };

	// Use this for initialization
	void Awake () {
        //int id = Array.IndexOf(expTarget, 505);

        GM = this;
    }

    void Start() {
        //StartCoroutine("SpawnFarmPopulate");
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void ButtonRevenge() {
        NetworkManager.nickname = panelLose.transform.GetChild(1).GetComponent<InputField>().text;
        NetworkManager.instance.AttempConnect();
    }

    IEnumerator SpawnFarmPopulate() {
        while(true) {
            if (farmParent.childCount <= 193)
                SpawnPopulationFarm();

            yield return new WaitForSeconds(3f);
        }
    }

    void SpawnPopulationFarm() {
        float xCenter = UnityEngine.Random.Range(-50, 50);
        float yCenter = UnityEngine.Random.Range(-45, 45);

        //Spawn 7 farm
        for(int i = 0; i < 7; i++) {
            Farm_Net f = farmPrefab[UnityEngine.Random.Range(0, farmPrefab.Length)];

            float x = UnityEngine.Random.Range(xCenter - 6, xCenter + 6);
            float y = UnityEngine.Random.Range(yCenter - 6, yCenter + 6);

            Instantiate(f, new Vector2(x, y), Quaternion.identity, farmParent);
        }
    }
}
