﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour {
    public InputField input;

    public void ButtonJoin() {
        NetworkManager.nickname = input.text;

        //Connecting to server
        NetworkManager.instance.AttempConnect();
    }

}
