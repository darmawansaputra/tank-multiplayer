﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour {
    public static string nickname;
    public static NetworkManager instance;
    public GameObject playerTank;

    ClientUDP client;

    private void Awake() {
        DontDestroyOnLoad(this);

        instance = this;
    }

    // Use this for initialization
    void Start () {
        client = GetComponent<ClientUDP>();
	}

    public void AttempConnect() {
        SceneManager.LoadScene("Gameplay");
        client.SendJoin(nickname);
    }
	
	public void Movement(float xPos, float yPos, float zRot) {
        client.SendPositionAndRotation(xPos, yPos, zRot);
    }

    public void Fire(float xPos, float yPos, float zRot) {
        client.SendBullet(xPos, yPos, zRot);
    }

    public void Upgrade(int target) {
        client.SendUpgrade(target);
    }

    private void OnApplicationQuit() {
        client.Close();
    }
}
