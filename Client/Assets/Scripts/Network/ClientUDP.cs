﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

//Type of packet transfer from server
public enum ServerPackets {
    S_JOIN = 1,
    S_TRANSFORM,
    S_STARTPOS,
    S_FIRE,
    S_BULLETRESET,
    S_FARMDATA,
    S_FARMRESET,
    S_LEVELUP,
    S_EXPERIENCE,
    S_TANKSTATS,
    S_TANKHEALTH,
    S_PLAYERLOSE,
    S_INITFARM,
};

//Type of packet transfer from client
public enum ClientPackets {
    S_JOIN = 1,
    S_TRANSFORM,
    S_DISCONNECT,
    S_FIRE,
    S_UPGRADE,
};

public class ClientUDP : MonoBehaviour {

    public static ClientUDP instance;
    public string ipServer = "127.0.0.1";
    public int portServer = 2018;

    public Byte[] receivedByte;

    bool handleData = false;

    UdpClient socket;

    private void Awake() {
        InitializeUDP();

        instance = this;

        //JoinGame();
    }

    private void Update() {
        if(handleData == true) {
            BufferByte bas = new BufferByte();
            bas.WriteBytes(receivedByte);

            Debug.Log("Iden baru guys: " + bas.ReadLong());

            HandlePackets.HandleData(receivedByte);

            handleData = false;

            //Receive again
            socket.BeginReceive(OnReceive, null);
        }
    }

    public void InitializeUDP() {
        socket = new UdpClient(ipServer, portServer);

        socket.BeginReceive(OnReceive, null);
    }

    private void OnReceive(IAsyncResult result) {

        IPEndPoint endPoint = null;
        receivedByte = socket.EndReceive(result, ref endPoint);

        //To handle type of packet
        handleData = true;
    }

    public void SendJoin(string nick) {
        BufferByte buffer = new BufferByte();

        buffer.WriteLong((long)ClientPackets.S_JOIN);
        buffer.WriteString(nick);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void SendPositionAndRotation(float xPos, float yPos, float zRot) {
        BufferByte buffer = new BufferByte();

        buffer.WriteLong((long)ClientPackets.S_TRANSFORM);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void SendBullet(float xPos, float yPos, float zRot) {
        BufferByte buffer = new BufferByte();

        buffer.WriteLong((long)ClientPackets.S_FIRE);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }
    
    public void SendUpgrade(int target) {
        BufferByte buffer = new BufferByte();

        buffer.WriteLong((long)ClientPackets.S_UPGRADE);
        buffer.WriteInteger(target);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void JoinGame() {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ClientPackets.S_JOIN);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void Close() {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ClientPackets.S_DISCONNECT);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);

        socket.Close();
        socket = null;
    }


}
