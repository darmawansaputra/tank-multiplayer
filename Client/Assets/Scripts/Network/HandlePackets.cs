﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class HandlePackets : MonoBehaviour {
    public GameObject tankPrefab;
    public Dictionary<int, GameObject> anotherTank = new Dictionary<int, GameObject>();

    private delegate void Packet_(byte[] data);
    public static BufferByte playerBuffer;
    private static Dictionary<long, Packet_> packets;
    private static long pLength;

    private void Awake() {
        InitializePackets();
    }

    private void InitializePackets() {
        packets = new Dictionary<long, Packet_>();
        packets.Add((long)ServerPackets.S_JOIN, PACKET_JOIN);
        packets.Add((long)ServerPackets.S_TRANSFORM, PACKET_TRANSFORM);
        packets.Add((long)ServerPackets.S_STARTPOS, PACKET_STARTPOS);
        packets.Add((long)ServerPackets.S_FIRE, PACKET_FIRE);
        packets.Add((long)ServerPackets.S_BULLETRESET, PACKET_BULLETRESET);
        packets.Add((long)ServerPackets.S_FARMDATA, PACKET_FARMDATA);
        packets.Add((long)ServerPackets.S_FARMRESET, PACKET_FARMRESET);
        packets.Add((long)ServerPackets.S_LEVELUP, PACKET_LEVELUP);
        packets.Add((long)ServerPackets.S_EXPERIENCE, PACKET_EXPERIENCE);
        packets.Add((long)ServerPackets.S_TANKSTATS, PACKET_TANKSTATS);
        packets.Add((long)ServerPackets.S_TANKHEALTH, PACKET_TANKHEALTH);
        packets.Add((long)ServerPackets.S_PLAYERLOSE, PACKET_PLAYERLOSE);
        packets.Add((long)ServerPackets.S_INITFARM, PACKET_INITFARM);
    }

    public static void HandleData(byte[] data) {
        byte[] Buffer;
        Buffer = (byte[])data.Clone();

        if (playerBuffer == null) {
            playerBuffer = new BufferByte();
        }


        playerBuffer.WriteBytes(Buffer);

        if (playerBuffer.Count() == 0) {
            playerBuffer.Clear();
            return;
        }

        if (playerBuffer.Length() >= 8) {
            pLength = playerBuffer.ReadLong(false);

            if (pLength <= 0) {
                playerBuffer.Clear();
                return;
            }
        }

        //while (pLength > 0 & pLength <= playerBuffer.Length() - 8) {
        //if (pLength <= playerBuffer.Length() - 8) {
        playerBuffer.ReadLong();
        //data = playerBuffer.ReadBytes((int)pLength);

        HandleDataPackets(data);
        //}

        playerBuffer.Clear();
        return;

        /*pLength = 0;
        if (playerBuffer.Length() >= 8) {
            pLength = playerBuffer.ReadLong(false);
            if (pLength < 0) {
                playerBuffer.Clear();
                return;
            }
        }*/
        //}
    }

    public static void HandleDataPackets(byte[] data) {
        long packetIdentifier;
        BufferByte buffer;
        Packet_ packet;

        buffer = new BufferByte();
        buffer.WriteBytes(data);
        packetIdentifier = buffer.ReadLong();

        Debug.Log("identifier: " + packetIdentifier);

        buffer.Dispose();

        if (packets.TryGetValue(packetIdentifier, out packet)) {
            packet.Invoke(data);
        }

    }

    private void PACKET_JOIN(byte[] data) {
        //Read data
        BufferByte bb = new BufferByte();
        bb.WriteBytes(data);
        bb.ReadLong();
        //bb.ReadInteger();

        int id = bb.ReadInteger();
        float xPos = bb.ReadFloat();
        float yPos = bb.ReadFloat();
        float zRot = bb.ReadFloat();
        string nick = bb.ReadString();

        Debug.Log("Waktunya instance player lain! " + id + " dan ");
        GameObject go = Instantiate(tankPrefab, new Vector3(xPos, yPos, 0), Quaternion.Euler(0, 0, zRot));
        go.GetComponent<TankStats>().nickname = nick;
        anotherTank.Add(id, go);

        bb.Clear();
    }

    private void PACKET_TRANSFORM(byte[] data) {
        //Read data
        BufferByte bb = new BufferByte();
        bb.WriteBytes(data);
        bb.ReadLong();

        Debug.Log("GET Packet Transform");

        //Change position
        float xPos = bb.ReadFloat();
        float yPos = bb.ReadFloat();
        float zRot = bb.ReadFloat();
        int id = bb.ReadInteger();

        anotherTank[id].transform.position = new Vector3(xPos, yPos, anotherTank[id].transform.position.z);
        anotherTank[id].transform.rotation = Quaternion.Euler(anotherTank[id].transform.eulerAngles.x, anotherTank[id].transform.eulerAngles.y, zRot);

        bb.Clear();
    }

    private void PACKET_STARTPOS(byte[] data) {
        //Read data
        BufferByte bb = new BufferByte();
        bb.WriteBytes(data);
        bb.ReadLong();

        float xPos = bb.ReadFloat();
        float yPos = bb.ReadFloat();
        float zRot = bb.ReadFloat();

        Transform go = NetworkManager.instance.playerTank.transform;
        go.position = new Vector3(xPos, yPos, go.position.z);
        go.rotation = Quaternion.Euler(go.eulerAngles.x, go.eulerAngles.y, zRot);

        bb.Clear();
    }

    private void PACKET_FIRE(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        //Position spawn (x and y) with direction rotation (z)
        float xPos = buffer.ReadFloat();
        float yPos = buffer.ReadFloat();
        float zRot = buffer.ReadFloat();
        float sp = buffer.ReadFloat();
        int index = buffer.ReadInteger();

        //Init to screen
        BulletManager.instance.Setup(xPos, yPos, zRot, sp, index);

        buffer.Clear();
    }

    private void PACKET_BULLETMOVE(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        //new position
        float xPos = buffer.ReadFloat();
        float yPos = buffer.ReadFloat();
        float zRot = buffer.ReadFloat();
        int index = buffer.ReadInteger();

        Transform t = BulletManager.instance.GetBullet(index);
        t.position = new Vector3(xPos, yPos, 0);
        t.rotation = Quaternion.Euler(0, 0, zRot);

        buffer.Clear();
    }

    private void PACKET_BULLETRESET(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int index = buffer.ReadInteger();

        BulletManager.instance.ResetBullet(index);

        buffer.Clear();
    }

    private void PACKET_FARMDATA(byte[] data) {
        Debug.Log("PACKET DATA GAN");

        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        //new position
        int index = buffer.ReadInteger();
        float health = buffer.ReadFloat();
        float xPos = buffer.ReadFloat();
        float yPos = buffer.ReadFloat();
        float zRot = buffer.ReadFloat();

        FarmManager.instance.UpdateFarm(index, health, xPos, yPos, zRot);

        buffer.Clear();
    }

    private void PACKET_FARMRESET(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int index = buffer.ReadInteger();

        FarmManager.instance.ResetFarm(index);

        buffer.Clear();
    }

    private void PACKET_LEVELUP(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int id = buffer.ReadInteger();
        int level = buffer.ReadInteger();

        TankStats ts = anotherTank[id].GetComponent<TankStats>();
        ts.level = level;
        ts.ChangeInfo();

        buffer.Clear();
    }

    private void PACKET_TANKSTATS(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int id = buffer.ReadInteger();
        float healthRegen = buffer.ReadFloat();
        float maxHealth = buffer.ReadFloat();
        float bulletSpeed = buffer.ReadFloat();
        float reload = buffer.ReadFloat();
        string type = buffer.ReadString();

        TankStats ts;
        if (type.Equals("another"))
            ts = anotherTank[id].GetComponent<TankStats>();
        else if (type.Equals("self"))
            ts = NetworkManager.instance.playerTank.GetComponent<TankStats>();
        else
            return;

        ts.healthRegen = healthRegen;
        ts.maxHealth = maxHealth;
        ts.bulletSpeed = bulletSpeed;
        ts.reloadSpeed = reload;

        buffer.Clear();
    }

    private void PACKET_EXPERIENCE(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int level = buffer.ReadInteger();
        int point = buffer.ReadInteger();
        float exp = buffer.ReadFloat();
        float expTarget = buffer.ReadFloat();

        TankStats ts = NetworkManager.instance.playerTank.GetComponent<TankStats>();
        ts.level = level;
        ts.pointStat = point;
        ts.experience = exp;

        ts.ChangeInfo();
        UIManager.UM.UpdateUIProfile(level, exp, expTarget);
        UIManager.UM.UpdateUIUpgrade(point);

        buffer.Clear();
    }

    private void PACKET_TANKHEALTH(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int id = buffer.ReadInteger();
        float currentHealth = buffer.ReadFloat();
        string type = buffer.ReadString();

        TankStats ts;
        if (type.Equals("another"))
            ts = anotherTank[id].GetComponent<TankStats>();
        else if (type.Equals("self"))
            ts = NetworkManager.instance.playerTank.GetComponent<TankStats>();
        else
            return;

        ts.currentHealth = currentHealth;

        buffer.Clear();
    }

    private void PACKET_PLAYERLOSE(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int id = buffer.ReadInteger();
        string killed = buffer.ReadString();
        string type = buffer.ReadString();

        if (type.Equals("another")) {
            Destroy(anotherTank[id]);
            anotherTank.Remove(id);
        }
        else if (type.Equals("self")) {
            Destroy(NetworkManager.instance.playerTank);
            GameManager.GM.panelLose.transform.GetChild(0).GetComponent<Text>().text = "You Killed by " + killed;
            GameManager.GM.panelLose.SetActive(true);
        }
        else
            return;

        anotherTank = new Dictionary<int, GameObject>();

        buffer.Clear();
    }

    private void PACKET_INITFARM(byte[] data) {
        print("FARM INIT");
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        Transform t = FarmManager.instance.transform;
        for(int i = 0; i < t.childCount; i++) {
            int id = buffer.ReadInteger();
            int status = buffer.ReadInteger();
            float xPos = buffer.ReadFloat();
            float yPos = buffer.ReadFloat();
            float currentHealth = buffer.ReadFloat();

            t.GetChild(id).gameObject.SetActive(status == 1 ? true : false);
            t.GetChild(id).position = new Vector3(xPos, yPos, 0);
            t.GetChild(id).GetComponent<Farm>().currentHealth = currentHealth;

            if (currentHealth < t.GetChild(id).GetComponent<Farm>().health)
                t.GetChild(id).GetComponent<Farm>().ChangeBar();
        }
    }

}