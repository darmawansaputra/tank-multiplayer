﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour {

    public static BulletManager instance;
    public GameObject bulletPrefab;

    public void Awake() {
        instance = this;
    }

    public Transform GetBullet(int index) {
        for (int i = 0; i <= index; i++) {
            if(i > transform.childCount - 1)
                Instantiate(bulletPrefab, transform);
        }

        return transform.GetChild(index);
    }

    public void Setup(float xPos, float yPos, float zRot, float sp, int index) {
        Transform t = GetBullet(index);

        t.position = new Vector3(xPos, yPos, 0);
        t.rotation = Quaternion.Euler(0, 0, zRot);

        t.gameObject.SetActive(true);
        t.GetComponent<TankBullet>().speed = sp;
    }

    public void ResetBullet(int index) {
        Transform t = GetBullet(index);
        t.gameObject.SetActive(false);
        t.GetComponent<TankBullet>().speed = 0f;
    }

}
