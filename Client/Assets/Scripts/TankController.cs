﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour {
    public TankBullet bulletPrefab;
    public Transform bulletPoint;

    TankStats stats;
    float fireReloadCount = 0f;
    Vector3 lastPos;
    Vector3 lastRot;

	// Use this for initialization
	void Awake () {
        stats = GetComponent<TankStats>();

        NetworkManager.instance.playerTank = this.gameObject;

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        stats.nickname = NetworkManager.nickname;
    }

	void Update () {
        //Movement
        lastPos = transform.position;
        lastRot = transform.eulerAngles;

        float x = Input.GetAxis("Horizontal") * Time.deltaTime * stats.movementSpeed;
        float y = Input.GetAxis("Vertical") * Time.deltaTime * stats.movementSpeed;

        transform.Translate(x, y, 0, Space.World);

        //Rotation
        Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float angle = Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x);
        float angleDeg = (180 / Mathf.PI) * angle;

        transform.rotation = Quaternion.Euler(0, 0, angleDeg - 90f);

        //If movement or rotate then send udp packet
        //if (transform.position != lastPos || transform.eulerAngles != lastRot) {
        if (transform.position != lastPos) {
            NetworkManager.instance.Movement(transform.position.x, transform.position.y, transform.eulerAngles.z);
        }

        if (Input.GetMouseButton(0) && fireReloadCount <= 0f) {
            fireReloadCount = stats.reloadSpeed;
            Fire();
        }
        fireReloadCount -= Time.deltaTime;
    }

    void Fire() {
        Debug.Log("FIRE!!");
            /*TankBullet blt = BulletManager.instance.GetBullet(bulletPoint.position, transform.rotation).GetComponent<TankBullet>();
            blt.speed = stats.bulletSpeed;
            blt.damage = stats.bulletDamage;
            blt.gameObject.SetActive(true);
            
            //Destroy(tb.gameObject, stats.bulletDurability);
            fireReloadCount = stats.reloadSpeed;*/

        NetworkManager.instance.Fire(bulletPoint.position.x, bulletPoint.position.y, transform.eulerAngles.z);
    }
    
}
