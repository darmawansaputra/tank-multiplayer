﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Farm_Net : NetworkBehaviour {
    public float health;
    
    public float exp;
    public float forcePower;
    public Image healthBar;

    [SyncVar]
    float currentHealth;

    Rigidbody2D rb;

    // Use this for initialization
    void Start() {
        currentHealth = health;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if(currentHealth < health) {
            transform.GetChild(0).gameObject.SetActive(true);
            healthBar.fillAmount = currentHealth / health;
        }
    }

    public float Hit(float dmg, Vector2 dir) {
        print("Hit Farm");

        currentHealth -= dmg;
        
        //rb.AddForce(dir * 5);

        if (currentHealth <= 0) {
            NetworkServer.Destroy(gameObject);
            return exp;
        }

        return 0f;
    }
}
