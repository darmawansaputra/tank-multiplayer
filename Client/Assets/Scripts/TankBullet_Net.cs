﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankBullet_Net : NetworkBehaviour {
    public float speed = 8f;
    public float damage = 0f;
    public float durability = 3f;

    public TankStats_Net tankParent;

    float lifeCounter = 0f;

    // Use this for initialization
    void Start() {

    }

    [ServerCallback]
    void Update() {
        lifeCounter += Time.deltaTime;
        if (lifeCounter > durability) {
            //Destroy on all client
            NetworkServer.Destroy(gameObject);
        }
    }

    void LateUpdate() {
        transform.Translate(0, speed * Time.deltaTime, 0);
    }    

    void OnTriggerEnter2D(Collider2D collision) {
        if (!isServer)
            return;

        if (collision.tag == "Collider") {
            NetworkServer.Destroy(gameObject);
        }
        else if (collision.tag == "Farm") {
            Vector2 dir = (collision.transform.position - transform.position).normalized;
            NetworkServer.Destroy(gameObject);
            
            tankParent.AddExp(collision.GetComponent<Farm_Net>().Hit(damage, dir));
        }
    }
}
