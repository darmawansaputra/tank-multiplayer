﻿using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour {
    public Slider sMaxHealth;
    public Slider sHealthRegen;
    public Slider sBodyDamage;
    public Slider sBulletDamage;
    public Slider sBulletDurability;
    public Slider sBulletSpeed;
    public Slider sReload;
    public Slider sMovement;

    TankStats tankStats;

    private void Start() {
        tankStats = NetworkManager.instance.playerTank.GetComponent<TankStats>();
    }

    public void UPMaxHealth() {
        if (tankStats.pointStat < 1 || sMaxHealth.value >= 6 || tankStats == null)
            return;

        sMaxHealth.value++;
        SendUpgrade(1);
    }

    public void UPHealthRegen() {
        if (tankStats.pointStat < 1 || sHealthRegen.value >= 6 || tankStats == null)
            return;

        sHealthRegen.value++;
        SendUpgrade(2);
    }

    public void UPBodyDamage() {
        if (tankStats.pointStat < 1 || sBodyDamage.value >= 6 || tankStats == null)
            return;

        sBodyDamage.value++;
        SendUpgrade(3);
    }

    public void UPBulletDamage() {
        if (tankStats.pointStat < 1 || sBulletDamage.value >= 6 || tankStats == null)
            return;

        sBulletDamage.value++;
        SendUpgrade(4);
    }

    public void UPBulletDurability() {
        if (tankStats.pointStat < 1 || sBulletDurability.value >= 6 || tankStats == null)
            return;

        sBulletDurability.value++;
        SendUpgrade(5);
    }

    public void UPBulletSpeed() {
        if (tankStats.pointStat < 1 || sBulletSpeed.value >= 6 || tankStats == null)
            return;

        sBulletSpeed.value++;
        SendUpgrade(6);
    }

    public void UPReload() {
        if (tankStats.pointStat < 1 || sReload.value >= 6 || tankStats == null)
            return;

        sReload.value++;
        SendUpgrade(7);
    }

    public void UPMovement() {
        if (tankStats.pointStat < 1 || sMovement.value >= 6 || tankStats == null)
            return;

        sMovement.value++;
        SendUpgrade(8);
    }

    public void SendUpgrade(int target) {
        tankStats.pointStat--;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
        NetworkManager.instance.Upgrade(target);
    }

    
}
