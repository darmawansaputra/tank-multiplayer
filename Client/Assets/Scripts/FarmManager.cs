﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FarmManager : MonoBehaviour {

    public static FarmManager instance;

    public Dictionary<int, Farm> objAvailable = new Dictionary<int, Farm>();

    private void Awake() {
        instance = this;
    }

    private void Start() {
        //register all farm to obj available
        /*for (int i = 0; i < transform.childCount; i++) {
            objAvailable.Add(i, transform.GetChild(i).GetComponent<Farm>());
        }*/
    }

    public int GetCountExists() {
        return objAvailable.Count;
    }

    public int GetAvailableFarm() {
        int index = -1;

        if (objAvailable.Count > 0)
            index = objAvailable.Keys.ToArray()[0];

        return index;
    }

    public void ResetFarm(int index) {
        Transform f = transform.GetChild(index);
        f.gameObject.SetActive(false);
        objAvailable.Add(index, f.GetComponent<Farm>());
    }

    public void UpdateFarm(int index, float health, float xPos, float yPos, float zRot) {
        Farm t = transform.GetChild(index).GetComponent<Farm>();

        t.currentHealth = health;
        t.transform.position = new Vector3(xPos, yPos, 0);
        t.transform.rotation = Quaternion.Euler(0, 0, zRot);

        t.Hit(0, Vector2.zero);
    }
}
