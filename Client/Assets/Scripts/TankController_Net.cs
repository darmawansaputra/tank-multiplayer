﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankController_Net : NetworkBehaviour {
    /*public TankBullet_Net bulletPrefab;
    public Transform bulletPoint;

    TankStats_Net stats;
    float fireReloadCount = 0f;

    // Use this for initialization
    void Awake() {
        stats = GetComponent<TankStats_Net>();
    }

    void Start() {
        if (!isLocalPlayer) {
            //Destroy(this);
            enabled = false;
            return;
        }

        GameManager.GM.tankStats = GetComponent<TankStats_Net>();
        FindObjectOfType<CameraController>().target = transform;
    }

    // Update is called once per frame
    void Update() {
        if (!isLocalPlayer)
            return;

        //Movement
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * stats.movementSpeed;
        float y = Input.GetAxis("Vertical") * Time.deltaTime * stats.movementSpeed;

        transform.Translate(x, y, 0, Space.World);

        //Rotation
        Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float angle = Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x);
        float angleDeg = (180 / Mathf.PI) * angle;

        transform.rotation = Quaternion.Euler(0, 0, angleDeg - 90f);

        if (Input.GetMouseButton(0) && fireReloadCount <= 0f) {
            CmdSpawnBullet();
            fireReloadCount = stats.reloadSpeed;
        }
        fireReloadCount -= Time.deltaTime;
    }

    [Command]
    void CmdSpawnBullet() {
        bulletPrefab.speed = stats.bulletSpeed;
        bulletPrefab.damage = stats.bulletDamage;
        bulletPrefab.durability = stats.bulletDurability;
        bulletPrefab.tankParent = stats;

        TankBullet_Net tb = Instantiate(bulletPrefab, bulletPoint.position, transform.rotation);
        //Destroy(tb.gameObject, stats.bulletDurability);

        NetworkServer.Spawn(tb.gameObject);
    }*/

}
