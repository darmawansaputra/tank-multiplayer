﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;

public class TankStats_Net : NetworkBehaviour {
    [SyncVar]
    public float movementSpeed = 4f;

    [SyncVar]
    public float bulletSpeed = 8f;

    [SyncVar]
    public float reloadSpeed = 0.75f;

    [SyncVar]
    public float bulletDurability = 1f;

    [SyncVar]
    public float bulletDamage = 10f;

    [SyncVar]
    public float healthRegen = 1f;

    [SyncVar]
    public float maxHealth = 100f;

    [SyncVar]
    public float bodyDamage = 0.5f;

    [SyncVar]
    public float experience = 0f;

    [SyncVar]
    public int lastExpIndex = -1;

    [SyncVar]
    public int level = 1;

    [SyncVar]
    public int pointStat = 0;

    public Image healthBar;

    float currentHealth;

    GameManager GM;

    // Use this for initialization
    void Start() {
        GM = GameManager.GM;
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update() {
        if (currentHealth < maxHealth) {
            currentHealth += healthRegen * Time.deltaTime;
            healthBar.fillAmount = currentHealth / maxHealth;

            if (!healthBar.transform.parent.gameObject.activeSelf)
                healthBar.transform.parent.gameObject.SetActive(true);

            if (currentHealth >= maxHealth)
                healthBar.transform.parent.gameObject.SetActive(false);
        }
    }

    void LateUpdate() {
        healthBar.transform.parent.parent.position = new Vector2(transform.position.x, transform.position.y - 0.85f);
        healthBar.transform.parent.parent.rotation = Quaternion.Euler(Vector2.zero);
    }

    public void AddExp(float xp) {
        if (level >= 29 || xp <= 0)
            return;

        experience += xp;
        bool next = false;

        do {
            next = false;

            if (experience >= GM.expTarget[lastExpIndex + 1]) {
                level++;
                lastExpIndex++;
                pointStat++;
                next = true;

                print("Level Up To " + level);
            }
        }
        while (next);

        //RpcUpdateUI();
    }

    public void UpdateUI() {
        print("MY EXP: " + experience);

        UIManager.UM.UpdateUIProfile(level, experience, GM.expTarget[lastExpIndex + 1]);
        UIManager.UM.UpdateUIUpgrade(pointStat);
    }

    //Decrease the hit point
    void OnCollisionStay2D(Collision2D collision) {
        if (collision.transform.tag == "Farm") {
            //Decrease the farm health
            Vector2 dir = (collision.transform.position - transform.position).normalized;
            AddExp(collision.transform.GetComponent<Farm_Net>().Hit(bodyDamage * Time.deltaTime, dir));

            //Decrease the tank health
            currentHealth -= (bodyDamage * 2 / 3) * Time.deltaTime;

            if (currentHealth <= 0f) {
                SceneManager.LoadScene(0);
            }
        }
    }

    [Command]
    public void CmdUpgrade(string type) {
        pointStat--;

        if (type == "maxHealth")
            maxHealth += 50f;
        else if(type == "healthRegen")
            healthRegen += 2f;
        else if (type == "bodyDamage")
            bodyDamage += 5f;
        else if (type == "bulletDamage")
            bulletDamage += 5f;
        else if (type == "bulletDurability")
            bulletDurability += 3f;
        else if (type == "bulletSpeed")
            bulletSpeed += 2f;
        else if (type == "reloadSpeed")
            reloadSpeed -= 0.05f;
        else if (type == "movementSpeed")
            movementSpeed += 0.35f;
    }
}
