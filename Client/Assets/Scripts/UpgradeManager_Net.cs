﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class UpgradeManager_Net : NetworkBehaviour {
    /*public Slider sMaxHealth;
    public Slider sHealthRegen;
    public Slider sBodyDamage;
    public Slider sBulletDamage;
    public Slider sBulletDurability;
    public Slider sBulletSpeed;
    public Slider sReload;
    public Slider sMovement;

    TankStats_Net tankStats;

    void LateUpdate() {
        if (tankStats == null)
            tankStats = GameManager.GM.tankStats;
    }

    [Command]
    public void CmdUPMaxHealth() {
        if (tankStats.pointStat < 1 || sMaxHealth.value >= 6 || tankStats == null)
            return;

        sMaxHealth.value++;
        tankStats.pointStat--;
        tankStats.maxHealth += 50f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPHealthRegen() {
        if (tankStats.pointStat < 1 || sHealthRegen.value >= 6 || tankStats == null)
            return;

        sHealthRegen.value++;
        tankStats.pointStat--;
        tankStats.healthRegen += 2f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPBodyDamage() {
        if (tankStats.pointStat < 1 || sBodyDamage.value >= 6 || tankStats == null)
            return;

        sBodyDamage.value++;
        tankStats.pointStat--;
        tankStats.bodyDamage += 5f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPBulletDamage() {
        if (tankStats.pointStat < 1 || sBulletDamage.value >= 6 || tankStats == null)
            return;

        print("HAI");

        sBulletDamage.value++;
        tankStats.pointStat--;
        tankStats.bulletDamage += 5f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPBulletDurability() {
        if (tankStats.pointStat < 1 || sBulletDurability.value >= 6 || tankStats == null)
            return;

        sBulletDurability.value++;
        tankStats.pointStat--;
        tankStats.bulletDurability += 3f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPBulletSpeed() {
        if (tankStats.pointStat < 1 || sBulletSpeed.value >= 6 || tankStats == null)
            return;

        sBulletSpeed.value++;
        tankStats.pointStat--;
        tankStats.bulletSpeed += 2f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPReload() {
        if (tankStats.pointStat < 1 || sReload.value >= 6 || tankStats == null)
            return;

        sReload.value++;
        tankStats.pointStat--;
        tankStats.reloadSpeed -= 0.05f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }

    [Command]
    public void CmdUPMovement() {
        if (tankStats.pointStat < 1 || sMovement.value >= 6 || tankStats == null)
            return;

        sMovement.value++;
        tankStats.pointStat--;
        tankStats.movementSpeed += 0.35f;
        UIManager.UM.UpdateUIUpgrade(tankStats.pointStat);
    }*/
}
