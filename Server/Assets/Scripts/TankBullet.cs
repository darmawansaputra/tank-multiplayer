﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBullet : MonoBehaviour {
    public float speed = 8f;
    public float damage = 0f;
    public float durability = 3f;
    public int id = -1;

    float lifeCounter = 0f;
    float syncCounter = 0f;

	// Use this for initialization
	void Start () {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }
	
	// Update is called once per frame
	void Update () {
        if (!gameObject.activeSelf)
            return;

        transform.Translate(0, speed * Time.deltaTime, 0);
        
        lifeCounter += Time.deltaTime;
        if(lifeCounter > durability) {
            //send deactivated gameobject bullet and reset
            lifeCounter = 0f;
            BulletManager.instance.ResetBullet(transform.GetSiblingIndex());
        }
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Collider") {
            lifeCounter = 0f;
            BulletManager.instance.ResetBullet(transform.GetSiblingIndex());
        }
        else if(collision.tag == "Player") {
            TankStats ts = collision.GetComponent<TankStats>();

            //Jika tank yg dikenai id tidak sama dgn id bullet
            if (ts.id != id) {
                //Decrease player health
                ts.currentHealth -= damage;

                //Broadcast new health player to all player
                //Jika health masih ada, maka kurangi dan broadcast current health
                if (ts.currentHealth > 0f)
                    NetworkManager.instance.DataTankHealth(ts.id, ts.currentHealth);

                //Jika sudah habis maka broadcast dia tewas dan setup socket close
                else {
                    NetworkManager.instance.OnPlayerKilled(ts, id);
                }

                lifeCounter = 0f;
                BulletManager.instance.ResetBullet(transform.GetSiblingIndex());
            }
        }
        else if(collision.tag == "Farm") {
            Vector2 dir = (collision.transform.position - transform.position).normalized;
            float xp = collision.GetComponent<Farm>().Hit(damage, dir);

            //Gimana caranya dalam satu paket mengirim 2 tipe perintah explore yg kemarin mantap
            //Delete bullet dan mengurangi darah farm / mendisablenya

            if (HandlePackets.anotherTank.ContainsKey(id)) {
                HandlePackets.anotherTank[id].GetComponent<TankStats>().AddExp(xp, id);
            }

            lifeCounter = 0f;
            BulletManager.instance.ResetBullet(transform.GetSiblingIndex());
        }
    }
}
