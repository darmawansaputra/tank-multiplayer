﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TankStats : MonoBehaviour {
    public int id = -1;
    public string nickname = "Unknown";
    public float movementSpeed = 4f;
    public float bulletSpeed = 8f;
    public float reloadSpeed = 0.75f;
    public float bulletDurability = 1f;
    public float bulletDamage = 10f;
    public float healthRegen = 1f;
    public float maxHealth = 100f;
    public float bodyDamage = 0.5f;

    public float experience = 0f;
    public int lastExpIndex = -1;
    public int level = 1;
    public int pointStat = 0;

    public Image healthBar;

    public float currentHealth;

    GameManager GM;

    // Use this for initialization
    void Start () {
        GM = GameManager.GM;
        currentHealth = maxHealth;

        ChangeInfo();
    }

    public void ChangeInfo() {
        healthBar.transform.parent.parent.GetChild(0).GetComponent<Text>().text = "Lv " + level + " - " + nickname;
    }

    // Update is called once per frame
    void Update () {
		if(currentHealth < maxHealth) {
            currentHealth += healthRegen * Time.deltaTime;
            healthBar.fillAmount = currentHealth / maxHealth;

            if(!healthBar.transform.parent.gameObject.activeSelf)
                healthBar.transform.parent.gameObject.SetActive(true);

            if (currentHealth >= maxHealth)
                healthBar.transform.parent.gameObject.SetActive(false);
        }
	}

    void LateUpdate() {
        healthBar.transform.parent.parent.position = new Vector2(transform.position.x, transform.position.y - 0.95f);
        healthBar.transform.parent.parent.rotation = Quaternion.Euler(Vector2.zero);
    }

    public void AddExp(float xp, int id) {
        if (level >= 29 || xp <= 0)
            return;

        experience += xp;
        bool next = false;
        bool isLevelUp = false;

        do {
            next = false;

            if(experience >= GM.expTarget[lastExpIndex + 1]) {
                level++;
                lastExpIndex++;
                pointStat++;
                next = true;
                isLevelUp = true;
            }
        }
        while (next);

        //Kirim ke player
        NetworkManager.instance.DataExperience(id, level, pointStat, experience, GM.expTarget[lastExpIndex + 1]);

        //Jika level up, broadcast ke yg lainnya
        if (isLevelUp) {
            ChangeInfo();
            NetworkManager.instance.DataLevelUp(id, level);
        }

        //UIManager.UM.UpdateUIProfile(level, experience, GM.expTarget[lastExpIndex + 1]);
        //UIManager.UM.UpdateUIUpgrade(pointStat);
    }

    //Decrease the hit point
    /*void OnCollisionStay2D(Collision2D collision) {
        if (collision.transform.tag == "Farm") {
            //Decrease the farm health
            /*Vector2 dir = (collision.transform.position - transform.position).normalized;
            collision.transform.GetComponent<Farm>().Hit(bodyDamage * Time.deltaTime, dir);*/
 
            //Decrease the tank health
            //currentHealth -= (bodyDamage * 2 / 3) * Time.deltaTime;

            /*if(currentHealth <= 0f) {
                SceneManager.LoadScene(0);
            }
        }
    }*/
}
