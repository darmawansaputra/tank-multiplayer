﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class HandlePackets : MonoBehaviour {
    public GameObject tankPrefab;
    public static Dictionary<int, GameObject> anotherTank = new Dictionary<int, GameObject>();

    private delegate void Packet_(byte[] data);
    public static BufferByte playerBuffer;
    private static Dictionary<long, Packet_> packets;
    private static long pLength;

    private void Awake() {
        InitializePackets();
    }

    private void InitializePackets() {
        packets = new Dictionary<long, Packet_>();
        packets.Add((long)ClientPackets.S_JOIN, PACKET_JOIN);
        packets.Add((long)ClientPackets.S_TRANSFORM, PACKET_TRANSFORM);
        packets.Add((long)ClientPackets.S_DISCONNECT, PACKET_DC);
        packets.Add((long)ClientPackets.S_FIRE, PACKET_FIRE);
        packets.Add((long)ClientPackets.S_UPGRADE, PACKET_UPGRADE);
    }

    public static void HandleData(byte[] data) {
        byte[] Buffer;
        Buffer = (byte[])data.Clone();

        if (playerBuffer == null) {
            playerBuffer = new BufferByte();
        }

        playerBuffer.WriteBytes(Buffer);

        if (playerBuffer.Count() == 0) {
            playerBuffer.Clear();
            return;
        }

        if (playerBuffer.Length() >= 8) {
            pLength = playerBuffer.ReadLong(false);

            if (pLength <= 0) {
                playerBuffer.Clear();
                return;
            }
        }

        //while (pLength > 0 & pLength <= playerBuffer.Length() - 8) {
        //if (pLength <= playerBuffer.Length() - 8) {
        playerBuffer.ReadLong();
        //data = playerBuffer.ReadBytes((int)pLength);

        HandleDataPackets(data);
        //}

        playerBuffer.Clear();
        return;

        /*pLength = 0;
        if (playerBuffer.Length() >= 8) {
            pLength = playerBuffer.ReadLong(false);
            if (pLength < 0) {
                playerBuffer.Clear();
                return;
            }
        }*/
        //}
    }

    public static void HandleDataPackets(byte[] data) {
        long packetIdentifier;
        BufferByte buffer;
        Packet_ packet;

        buffer = new BufferByte();
        buffer.WriteBytes(data);
        packetIdentifier = buffer.ReadLong();

        buffer.Dispose();

        if (packets.TryGetValue(packetIdentifier, out packet)) {
            Debug.Log("Identifier: " + packetIdentifier);
            packet.Invoke(data);
        }

    }

    private void PACKET_JOIN(byte[] data) {
        Debug.Log("Invoke: Packet Join");

        //Jika adalah klien baru maka ditambahkan ke array
        if (!ServerUDP.clientList.ContainsKey(ServerUDP.endPoint)) {
            ServerUDP.playerCounter++;

            BufferByte myBuff = new BufferByte();
            myBuff.WriteBytes(data);
            myBuff.ReadLong();
            string nick = myBuff.ReadString();

            Client temp = new Client();
            temp.playerID = ServerUDP.playerCounter;
            temp.nickname = nick;
            temp.ipEndPoint = ServerUDP.endPoint;
            temp.ip = ServerUDP.endPoint.Address.ToString();
            temp.position = new Vector3(ServerUDP.playerCounter * 2, 0, 0);
            temp.rotation = new Vector3(0, 0, 0);

            ServerUDP.clientList.Add(ServerUDP.endPoint, temp);
            Debug.Log("Client baru: " + ServerUDP.endPoint.Address.ToString() + " id: " + temp.playerID);

            BufferByte buff = new BufferByte();
            buff.WriteLong((long)ServerPackets.S_STARTPOS);
            buff.WriteFloat(temp.position.x);
            buff.WriteFloat(temp.position.y);
            buff.WriteFloat(temp.rotation.z);
            ServerUDP.instance.Send(ServerUDP.endPoint, buff.ToArray());

            //Farm initialize
            byte[] farm = FarmManager.instance.ParseBuffer();
            ServerUDP.instance.Send(ServerUDP.endPoint, farm);

            buff = new BufferByte();
            buff.WriteLong((long)ServerPackets.S_JOIN);
            buff.WriteInteger(temp.playerID);
            buff.WriteFloat(temp.position.x);
            buff.WriteFloat(temp.position.y);
            buff.WriteFloat(temp.rotation.z);
            buff.WriteString(nick);

            //Instante
            GameObject go = Instantiate(tankPrefab, new Vector3(temp.position.x, temp.position.y, 0), Quaternion.Euler(0, 0, temp.rotation.z));
            go.GetComponent<TankStats>().id = temp.playerID;
            go.GetComponent<TankStats>().nickname = nick;
            anotherTank.Add(temp.playerID, go);

            print("ID TANK: " + temp.playerID);

            //Broadcast new player to another player
            foreach (KeyValuePair<IPEndPoint, Client> e in ServerUDP.clientList) {
                try {
                    if (!e.Key.Equals(ServerUDP.endPoint)) {
                        ServerUDP.instance.Send(e.Key, buff.ToArray());
                    }
                }
                catch { }
            }

            //Send another data player to new player
            foreach (KeyValuePair<IPEndPoint, Client> e in ServerUDP.clientList) {
                try {
                    buff = new BufferByte();
                    Client c = ServerUDP.clientList[e.Key];
                    Transform tk = anotherTank[e.Value.playerID].transform;

                    if (!e.Key.Equals(ServerUDP.endPoint)) {
                        buff.WriteLong((long)ServerPackets.S_JOIN);
                        buff.WriteInteger(c.playerID);
                        buff.WriteFloat(tk.position.x);
                        buff.WriteFloat(tk.position.y);
                        buff.WriteFloat(tk.eulerAngles.z);
                        buff.WriteString(c.nickname);

                        ServerUDP.instance.Send(ServerUDP.endPoint, buff.ToArray());
                    }
                }
                catch { }
            }

        }
        else {

        }
    }

    private void PACKET_TRANSFORM(byte[] data) {
        Debug.Log("Invoke: Packet Transform");

        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        //Change position
        buffer.ReadLong();
        float xPos = buffer.ReadFloat();
        float yPos = buffer.ReadFloat();
        float zRot = buffer.ReadFloat();
        int id = ServerUDP.clientList[ServerUDP.endPoint].playerID;

        anotherTank[id].transform.position = new Vector3(xPos, yPos, anotherTank[id].transform.position.z);
        anotherTank[id].transform.rotation = Quaternion.Euler(anotherTank[id].transform.eulerAngles.x, anotherTank[id].transform.eulerAngles.y, zRot);

        //Create new again
        buffer = new BufferByte();
        buffer.WriteBytes(data);
        buffer.WriteInteger(ServerUDP.clientList[ServerUDP.endPoint].playerID);

        //Broadcast new transform to other player
        foreach (KeyValuePair<IPEndPoint, Client> e in ServerUDP.clientList) {
            try {
                if (!e.Key.Equals(ServerUDP.endPoint)) {
                    ServerUDP.instance.Send(e.Key, buffer.ToArray());
                }
            }
            catch { }
        }
    }

    private void PACKET_FIRE(byte[] data) {
        Debug.Log("Invoke: Packet Fire");

        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        //Position spawn (x and y) with direction rotation (z)
        float xPos = buffer.ReadFloat();
        float yPos = buffer.ReadFloat();
        float zRot = buffer.ReadFloat();

        int id = ServerUDP.clientList[ServerUDP.endPoint].playerID;
        TankStats tank = anotherTank[id].GetComponent<TankStats>();

        //Init to screen
        int index = BulletManager.instance.Setup(xPos, yPos, zRot, id, tank);

        //Write again to setup send all player
        buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_FIRE);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);
        buffer.WriteFloat(tank.bulletSpeed);
        buffer.WriteInteger(index);

        //Send process
        ServerUDP.instance.SendAll(buffer.ToArray());
    }

    private void PACKET_DC(byte[] data) {
        Debug.Log("Invoke: Packet DC");

        if(ServerUDP.clientList.ContainsKey(ServerUDP.endPoint)) {
            int id = ServerUDP.clientList[ServerUDP.endPoint].playerID;
            /*ServerUDP.clientList.Remove(ServerUDP.endPoint);*/

            NetworkManager.instance.OnPlayerKilled(anotherTank[id].GetComponent<TankStats>(), -1);
        }
    }

    private void PACKET_UPGRADE(byte[] data) {
        BufferByte buffer = new BufferByte();
        buffer.WriteBytes(data);

        buffer.ReadLong();

        int id = ServerUDP.clientList[ServerUDP.endPoint].playerID;
        int choose = buffer.ReadInteger();
        GameObject tank = anotherTank[id];

        GameManager.GM.DoUpgrade(tank, choose, id);
    }

}