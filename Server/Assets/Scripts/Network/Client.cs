﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Client : MonoBehaviour {
    public int playerID;
    public string nickname;
    public IPEndPoint ipEndPoint;
    public string ip;
    public Vector3 position;
    public Vector3 rotation;

    //Tank stats
    public float movementSpeed = 4f;
    public float bulletSpeed = 8f;
    public float reloadSpeed = 0.75f;
    public float bulletDurability = 1f;
    public float bulletDamage = 10f;
    public float healthRegen = 1f;
    public float maxHealth = 100f;
    public float bodyDamage = 0.5f;

    public float experience = 0f;
    public int lastExpIndex = -1;
    public int level = 1;
    public int pointStat = 0;
}