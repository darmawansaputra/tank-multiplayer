﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

//Type of packet transfer from server
public enum ServerPackets {
    S_JOIN = 1,
    S_TRANSFORM,
    S_STARTPOS,
    S_FIRE,
    S_BULLETRESET,
    S_FARMDATA,
    S_FARMRESET,
    S_LEVELUP,
    S_EXPERIENCE,
    S_TANKSTATS,
    S_TANKHEALTH,
    S_PLAYERLOSE,
    S_INITFARM,
};

//Type of packet transfer from client
public enum ClientPackets {
    S_JOIN = 1,
    S_TRANSFORM,
    S_DISCONNECT,     
    S_FIRE,
    S_UPGRADE,
};

public class ServerUDP : MonoBehaviour {
    public static ServerUDP instance;

    //public Socket socket;
    public bool isAlive = true;
    public UdpClient socket;
    public int port = 2018;
    public IPEndPoint ipEndPoint;

    public static int playerCounter = -1;
    public static Dictionary<IPEndPoint, Client> clientList = new Dictionary<IPEndPoint, Client>();
    public static IPEndPoint endPoint = null;

    Byte[] receivedByte;

    bool handleData = false;

    private void Awake() {
        instance = this;

        //Fungsi init pada server
        InitServer();
    }

    private void Update() {
        if (handleData == true) {
            HandlePackets.HandleData(receivedByte);

            handleData = false;

            //Receive again
            socket.BeginReceive(OnReceive, null);
        }
    }

    private void InitServer() {
        //Init server endpoint
        ipEndPoint = new IPEndPoint(IPAddress.Any, port);
        socket = new UdpClient(ipEndPoint);

        Debug.Log("Server up!...");

        //Start accept new client
        socket.BeginReceive(OnReceive, null);
    }

    public void Send(IPEndPoint endPoint, byte[] data) {
        socket.Send(data, data.Length, endPoint);
    }

    public void SendAll(byte[] data) {
        foreach (KeyValuePair<IPEndPoint, Client> e in clientList) {
            try {
                Send(e.Key, data);
            }
            catch { }
        }
    }

    public void SendAllExcept(byte[] data, int id) {
        foreach (KeyValuePair<IPEndPoint, Client> e in clientList) {
            try {
                if(e.Value.playerID != id)
                    Send(e.Key, data);
            }
            catch { }
        }
    }

    public void SendOnly(byte[] data, int id) {
        foreach (KeyValuePair<IPEndPoint, Client> e in clientList) {
            try {
                if (e.Value.playerID == id) {
                    Send(e.Key, data);
                    break;
                }
            }
            catch { }
        }
    }

    public void ClosePlayer(int id) {
        IPEndPoint ep = null;

        foreach (KeyValuePair<IPEndPoint, Client> e in clientList) {
            if (e.Value.playerID == id) {
                ep = e.Key;
                break;
            }
        }

        if (ep != null) {
            ServerUDP.clientList.Remove(ep);            
        }
    }

    private void OnReceive(IAsyncResult result) {
        receivedByte = socket.EndReceive(result, ref endPoint);

        handleData = true;
    }


}