﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour {
    public static NetworkManager instance;

    ServerUDP server;

    private void Awake() {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        server = GetComponent<ServerUDP>();
	}
	
	public void Movement(float xPos, float yPos, float zRot) {
        //server.SendPositionAndRotation(xPos, yPos, zRot);
    }

    public void BulletMovement(int index, float xPos, float yPos, float zRot) {
        /*BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_BULLETMOVE);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);
        buffer.WriteInteger(index);

        ServerUDP.instance.SendAll(buffer.ToArray());*/
    }

    public void BulletReset(int index) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_BULLETRESET);
        buffer.WriteInteger(index);

        ServerUDP.instance.SendAll(buffer.ToArray());
    }

    public void FarmReset(int index) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_FARMRESET);
        buffer.WriteInteger(index);

        ServerUDP.instance.SendAll(buffer.ToArray());
    }

    public void FarmData(int index, float health, float xPos, float yPos, float zRot) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_FARMDATA);
        buffer.WriteInteger(index);
        buffer.WriteFloat(health);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);

        ServerUDP.instance.SendAll(buffer.ToArray());
    }

    public void DataExperience(int id, int level, int point, float exp, float expTarget) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_EXPERIENCE);
        buffer.WriteInteger(level);
        buffer.WriteInteger(point);
        buffer.WriteFloat(exp);
        buffer.WriteFloat(expTarget);

        ServerUDP.instance.SendOnly(buffer.ToArray(), id);
    }

    public void DataLevelUp(int id, int level) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_LEVELUP);
        buffer.WriteInteger(id);
        buffer.WriteInteger(level);

        ServerUDP.instance.SendAllExcept(buffer.ToArray(), id);
    }

    public void DataTankStats(int id, float healthRegen, float maxHealth, float bulletSpeed, float reload) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_TANKSTATS);
        buffer.WriteInteger(id);
        buffer.WriteFloat(healthRegen);
        buffer.WriteFloat(maxHealth);
        buffer.WriteFloat(bulletSpeed);
        buffer.WriteFloat(reload);

        BufferByte clone = new BufferByte();
        clone.WriteBytes(buffer.ToArray());
        clone.WriteString("another");
        ServerUDP.instance.SendAllExcept(clone.ToArray(), id);

        clone = new BufferByte();
        clone.WriteBytes(buffer.ToArray());
        clone.WriteString("self");
        ServerUDP.instance.SendOnly(clone.ToArray(), id);
    }

    public void DataTankHealth(int id, float currentHealth) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_TANKHEALTH);
        buffer.WriteInteger(id);
        buffer.WriteFloat(currentHealth);

        BufferByte clone = new BufferByte();
        clone.WriteBytes(buffer.ToArray());
        clone.WriteString("another");
        ServerUDP.instance.SendAllExcept(clone.ToArray(), id);

        clone = new BufferByte();
        clone.WriteBytes(buffer.ToArray());
        clone.WriteString("self");
        ServerUDP.instance.SendOnly(clone.ToArray(), id);
    }

    public void DataLose(int id, string killed, bool me = true) {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_PLAYERLOSE);
        buffer.WriteInteger(id);
        buffer.WriteString(killed);

        BufferByte clone = new BufferByte();
        clone.WriteBytes(buffer.ToArray());
        clone.WriteString("another");
        ServerUDP.instance.SendAllExcept(clone.ToArray(), id);

        if(me) {
            clone = new BufferByte();
            clone.WriteBytes(buffer.ToArray());
            clone.WriteString("self");
            ServerUDP.instance.SendOnly(clone.ToArray(), id);
        }
        
    }

    public void OnPlayerKilled(TankStats ts, int id) {

        //Send to another player to remove also
        if (HandlePackets.anotherTank.ContainsKey(id))
            NetworkManager.instance.DataLose(ts.id, HandlePackets.anotherTank[id].GetComponent<TankStats>().nickname);
        else
            NetworkManager.instance.DataLose(ts.id, "?", false);

        //Remove gameobject
        Destroy(HandlePackets.anotherTank[ts.id]);
        HandlePackets.anotherTank.Remove(ts.id);

        //Close player socket
        ServerUDP.instance.ClosePlayer(ts.id);
    }

    private void OnApplicationQuit() {
        //server.Close();
    }

}
