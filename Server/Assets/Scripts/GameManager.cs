﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager GM;
    public TankStats tankStats;

    public Transform farmParent;
    public Farm[] farmPrefab;

    public float[] expTarget = {
        10f, 20f, 30f, 40f, 55f, 60f, 75f, 90f, 105f,
        135f, 165f, 195f, 225f, 255f, 285f, 315f, 345f, 375f, 405f,
        455f, 505f, 555f, 605f, 655f, 705f, 755f, 805f, 855f, 905f
    };

	// Use this for initialization
	void Awake () {
        //int id = Array.IndexOf(expTarget, 505);

        GM = this;
    }

    void Start() {
        //StartCoroutine("SpawnFarmPopulate");
    }

    // Update is called once per frame
    void Update () {
		
	}

    IEnumerator SpawnFarmPopulate() {
        while(true) {
            if (farmParent.childCount <= 193)
                SpawnPopulationFarm();

            yield return new WaitForSeconds(3f);
        }
    }

    void SpawnPopulationFarm() {
        float xCenter = UnityEngine.Random.Range(-50, 50);
        float yCenter = UnityEngine.Random.Range(-45, 45);

        //Spawn 7 farm
        for(int i = 0; i < 7; i++) {
            Farm f = farmPrefab[UnityEngine.Random.Range(0, farmPrefab.Length)];

            float x = UnityEngine.Random.Range(xCenter - 6, xCenter + 6);
            float y = UnityEngine.Random.Range(yCenter - 6, yCenter + 6);

            Instantiate(f, new Vector2(x, y), Quaternion.identity, farmParent);
        }
    }

    public void DoUpgrade(GameObject tank, int choose, int id) {
        TankStats ts = tank.GetComponent<TankStats>();
        
        if(ts.pointStat > 0) {
            if(choose == 1) {
                ts.maxHealth += 50f;
            }
            else if (choose == 2) {
                ts.healthRegen += 2f;
            }
            else if (choose == 3) {
                ts.bodyDamage += 5f;
            }
            else if (choose == 4) {
                ts.bulletDamage += 5f;
            }
            else if (choose == 5) {
                ts.bulletDurability += 3f;
            }
            else if (choose == 6) {
                ts.bulletSpeed += 2f;
            }
            else if (choose == 7) {
                ts.reloadSpeed -= 0.1f;
            }
            else if (choose == 8) {
                ts.movementSpeed += 0.35f;
            }

            ts.pointStat--;
            NetworkManager.instance.DataTankStats(id, ts.healthRegen, ts.maxHealth, ts.bulletSpeed, ts.reloadSpeed);
        }
    }
}
