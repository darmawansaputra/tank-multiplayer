﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FarmManager : MonoBehaviour {
    public static FarmManager instance;

    public Dictionary<int, Farm> objAvailable = new Dictionary<int, Farm>();

    private void Awake() {
        instance = this;
    }

    private void Start() {
        //register all farm to obj available
        /*for(int i = 0; i < transform.childCount; i++) {
            objAvailable.Add(i, transform.GetChild(i).GetComponent<Farm>());
        }*/
    }

    public int GetCountExists() {
        return objAvailable.Count;
    }

    public int GetAvailableFarm() {
        int index = -1;

        if (objAvailable.Count > 0)
            index = objAvailable.Keys.ToArray()[0];

        return index;
    }

    public void ResetFarm(int index) {
        Transform f = transform.GetChild(index);
        f.gameObject.SetActive(false);
        objAvailable.Add(index, f.GetComponent<Farm>());

        NetworkManager.instance.FarmReset(index);
    }

    public byte[] ParseBuffer() {
        BufferByte buffer = new BufferByte();
        buffer.WriteLong((long)ServerPackets.S_INITFARM);

        for(int i = 0; i < transform.childCount; i++) {
            Transform t = transform.GetChild(i);
            buffer.WriteInteger(i);
            buffer.WriteInteger(t.gameObject.activeSelf ? 1 : 0);
            buffer.WriteFloat(t.position.x);
            buffer.WriteFloat(t.position.y);
            buffer.WriteFloat(t.GetComponent<Farm>().currentHealth);
        }

        return buffer.ToArray();
    }

}
