﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Farm : MonoBehaviour {
    public float health;
    public float exp;
    public float forcePower;
    public Image healthBar;

    public float currentHealth;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        currentHealth = health;
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public float Hit(float dmg, Vector2 dir) {
        print("Hit Farm");

        currentHealth -= dmg;
        transform.GetChild(0).gameObject.SetActive(true);
        healthBar.fillAmount = currentHealth / health;
        //rb.AddForce(dir * 5);

        NetworkManager.instance.FarmData(transform.GetSiblingIndex(), currentHealth, transform.position.x, transform.position.y, transform.eulerAngles.z);

        if(currentHealth <= 0) {
            //Destroy(gameObject);
            FarmManager.instance.ResetFarm(transform.GetSiblingIndex());
            return exp;
        }

        return 0f;
    }
}
