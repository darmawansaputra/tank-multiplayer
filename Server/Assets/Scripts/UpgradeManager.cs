﻿using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour {
    public Slider sMaxHealth;
    public Slider sHealthRegen;
    public Slider sBodyDamage;
    public Slider sBulletDamage;
    public Slider sBulletDurability;
    public Slider sBulletSpeed;
    public Slider sReload;
    public Slider sMovement;

    TankStats tankStats;

    void LateUpdate() {
        if(tankStats == null)
            tankStats = GameManager.GM.tankStats;
    }

    public void UPMaxHealth() {
        if (tankStats.pointStat < 1 || sMaxHealth.value >= 6 || tankStats == null)
            return;

        sMaxHealth.value++;
    }

    public void UPHealthRegen() {
        if (tankStats.pointStat < 1 || sHealthRegen.value >= 6 || tankStats == null)
            return;

        sHealthRegen.value++;
    }

    public void UPBodyDamage() {
        if (tankStats.pointStat < 1 || sBodyDamage.value >= 6 || tankStats == null)
            return;

        sBodyDamage.value++;
    }

    public void UPBulletDamage() {
        if (tankStats.pointStat < 1 || sBulletDamage.value >= 6 || tankStats == null)
            return;

        sBulletDamage.value++;
    }

    public void UPBulletDurability() {
        if (tankStats.pointStat < 1 || sBulletDurability.value >= 6 || tankStats == null)
            return;

        sBulletDurability.value++;
    }

    public void UPBulletSpeed() {
        if (tankStats.pointStat < 1 || sBulletSpeed.value >= 6 || tankStats == null)
            return;

        sBulletSpeed.value++;
    }

    public void UPReload() {
        if (tankStats.pointStat < 1 || sReload.value >= 6 || tankStats == null)
            return;

        sReload.value++;
    }

    public void UPMovement() {
        if (tankStats.pointStat < 1 || sMovement.value >= 6 || tankStats == null)
            return;

        sMovement.value++;
    }

    
}
