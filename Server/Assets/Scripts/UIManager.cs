﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public static UIManager UM;
    public Text txtPointAvailable;
    public Text txtProfile;
    public Slider sliderExp;

    TankStats stats;

    private void Start() {
        UM = this;
    }

    void LateUpdate() {
        if (stats != null) {
            UpdateUIUpgrade(stats.pointStat);
            UpdateUIProfile(stats.level, stats.experience, GameManager.GM.expTarget[stats.lastExpIndex + 1]);
        }
        else
            stats = GameManager.GM.tankStats;
    }

    public void UpdateUIUpgrade(int point) {
        txtPointAvailable.text = "Point Available: " + point;
    }

    public void UpdateUIProfile(int level, float exp, float maxExp) {
        txtProfile.text = "Tank Level " + level;
        sliderExp.value = exp;
        sliderExp.maxValue = maxExp;
    }
}
