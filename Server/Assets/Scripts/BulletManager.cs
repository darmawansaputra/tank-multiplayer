﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour {

    public static BulletManager instance;
    public GameObject bulletPrefab;

    public void Awake() {
        instance = this;
    }

    public int GetBulletIndex() {
        int index = -1;

        for(int i = 0; i < transform.childCount; i++) {
            if(!transform.GetChild(i).gameObject.activeSelf) {
                index = i;
                break;
            }
        }
        
        //Jika tidak ada yg kosong, then create new
        if(index == -1) {
            index = transform.childCount;
            Instantiate(bulletPrefab, transform);
        }

        return index;
    }

    public int Setup(float xPos, float yPos, float zRot, int id, TankStats player) {
        int index = GetBulletIndex();

        TankBullet go = transform.GetChild(index).GetComponent<TankBullet>();

        go.gameObject.transform.position = new Vector3(xPos, yPos, 0);
        go.gameObject.transform.rotation = Quaternion.Euler(0, 0, zRot);

        go.id = id;
        go.speed = player.bulletSpeed;
        go.damage = player.bulletDamage;
        go.durability = player.bulletDurability;

        go.gameObject.SetActive(true);

        return index;
    }

    public void ResetBullet(int index) {
        TankBullet go = transform.GetChild(index).GetComponent<TankBullet>();

        go.id = -1;
        go.speed = 0f;
        go.damage = 0f;
        go.durability = 0f;

        go.gameObject.SetActive(false);

        //Send to other player
        NetworkManager.instance.BulletReset(index);
    }

}
